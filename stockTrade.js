

function getStockTrades() {
    const url = "http://spring-hackathon-spring-hackathon.emeadocker12.conygre.com/api/v1/stock/";

    fetch(url)
        .then(response => {
            console.log(response);
            console.log(response.status);
            return response.json();
        })
        .then(stockJson => {
            console.log(stockJson);
            let htmlString = "";

            stockJson.forEach(stock => {
                htmlString += "<tr><td>" + stock.createdTimestamp + "</td>";
                htmlString += "<td>" + stock.id + "</td>";
                htmlString += "<td>" + stock.buyOrSell + "</td>";
                htmlString += "<td>" + stock.stockTicker + "</td>";
                htmlString += "<td>" + stock.price + "</td>";
                htmlString += "<td>" + stock.volume + "</td>";
                htmlString += "<td>" + stock.statusCode + "</td></tr>";
            });

            document.querySelector("#order-table").innerHTML = htmlString;
        });
}